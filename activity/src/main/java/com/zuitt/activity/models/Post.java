package com.zuitt.activity.models;
import javax.persistence.*;

@Entity
//Designates the table related to the model
@Table(name="posts")
public class Post {

    // Properties
    @Id
    //The values that belong to this property will be incremented
    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    // Constructors
    public Post(){};

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    // Getters and Setters
    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }
}

